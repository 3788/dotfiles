#!/bin/bash
echo "A script to replace the dotfiles with symlinks to the dotfiles folder"
cd "$HOME" || exit
files="alias vimrc zsh_plugins zshrc"
for FILE in $files
do
    if [ ! -f "$HOME/.$FILE" ]; then
        echo "~/.$FILE does not exist."
        read -r -p "Do you wish to create new symlink ~/.$FILE? [y/N] " response
        response=${response,,}  # tolower
        if [[ "$response" =~ ^(yes|y)$ ]]; then
            ln -s "dotfiles/$FILE" "$HOME/.$FILE"
        fi
    else
        echo "$FILE already exists."
        read -r -p "Do you wish to replace? [y/N] " response
        response=${response,,}  # tolower
        if [[ "$response" =~ ^(yes|y)$ ]]; then
            trash "$HOME/.$FILE"
            ln -s "dotfiles/$FILE" "$HOME/.$FILE"
        fi
    fi
done

