"First 3 lines are the minimum vimrc recommended by pathogen
execute pathogen#infect()
"syntax highlighting
syntax on
filetype plugin indent on
filetype on

"Needed to allow Vim to use true color
set termguicolors

"Set airline theme
" let g:airline_theme='soda'
let g:airline_theme='base16'
" let g:airline_theme='nord'
" let g:airline_theme='papercolor'
"Use powerline fonts for airline
let g:airline_powerline_fonts = 1 

set mouse=a
        
set tabstop=4       " number of visual spaces per TAB
set softtabstop=4   " number of spaces in tab when editing
set expandtab       " tabs are spaces
set shiftwidth=4

set number          " show line numbers
"set rnu             " set relative line numbers

set showcmd         " show command in bottom bar
set cursorline      " highlight current line

"set incsearch      " search as characters are entered
"set hlsearch       " highlight matches

" jk is escape. `^ jumps to mark at which cursor last was, when exiting i-mode.
inoremap jk <esc>`^

" These lines needed to allow Alt/Meta key with jk in terminal
" From here: https://newbedev.com/can-i-map-alt-key-in-vim
execute "set <M-j>=\ej"
execute "set <M-k>=\ek"
" Use Meta + jk to move lines
nnoremap <M-k> :m .-2<CR>==
nnoremap <M-j> :m .+1<CR>==
inoremap <M-j> <Esc>:m .+1<CR>==gi
inoremap <M-k> <Esc>:m .-2<CR>==gi
vnoremap <M-j> :m '>+1<CR>gv=gv
vnoremap <M-k> :m '<-2<CR>gv=gv

" Choose prolog as default for .pl files not perl
let g:filetype_pl="prolog"

" Syntastic settings
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0


"need to find a good colorscheme! 

" set background=dark
" colorscheme nord
hi Normal guibg=NONE ctermbg=NONE
colorscheme embark
" colorscheme embark_solarized 
" colorscheme iceberg 
" colorscheme tempus_future
" colorscheme PaperColor
" colorscheme tokyo-metro
" colorscheme atom
" colorscheme badwolf
" colorscheme industry
" colorscheme gruvbox
